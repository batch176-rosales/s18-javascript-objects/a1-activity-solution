//creating constructor
    function Pokemon(name,level){
        //properties
        this.name=name;
        this.level=level;
        this.health=3*level;
        this.attack=level
        //methods
        this.tackle= function(target){
            
            console.log(this.name + " tackled "  + target.name +".")
            console.log(target.name + "'s health now reduced to " +(target.health - this.attack)+".");
        
        };
        this.faint=function(target){
            target.health -=this.attack
            if(target.health<5){
                
            console.log(target.name + " fainted.")
            }

        }
        

    }
    let pikachu= new Pokemon("Pikachu",16);
    let charizard=new Pokemon("Charizard", 10);
    let hermie=new Pokemon("Hermie", 15);
  
    pikachu.tackle(charizard);
    pikachu.faint(charizard);
    pikachu.tackle(charizard);
    pikachu.faint(charizard);
    pikachu.tackle(hermie);
    pikachu.faint(hermie);
    pikachu.tackle(hermie);
    pikachu.faint(hermie);
    pikachu.tackle(hermie);
    pikachu.faint(hermie);
   
  